/*
1. Скачал плагин jquery.dataTables.min.js версии 1.10.10
https://datatables.net/examples/advanced_init/footer_callback.html

2. Вместо ',' поставил в таблице '.'

3. Переделал немного код
   Указал класс таблицы '.hours' 
   Округлил результат  toFixed(2) 
*/

$(document).ready(function() {
    $('.hours').DataTable( {  //<-hours class
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                '$'+pageTotal.toFixed(2) +' ( $'+ total.toFixed(2) +' total)' //<-toFixed(2)
            );
        }
    } );
} );